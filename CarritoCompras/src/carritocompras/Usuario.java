package carritocompras;

/**
 * Clase para usuarios
 * @author  Jessica Johana Estefanía Chin Pech 
 * @author  Leslie Alejandra Colli Pech 
 * @author  Karen Estefanía Pérez Pérez 
 * @author  Omar Jasiel Rodriguez Cab 
 * @author  Diana Carolina Uc Vazquez
 * @author  Universidad Autónoma de Campeche
 * @since   Junio 2020
 * @version 3.0
 */
public class Usuario {
    
    /**
     * Atributos (nombre, contraseña, saldo, tipo de cliente y decuento a aplicar)
     */
    private String nombre;
    private String password;
    private double saldoTarjeta;
    private String cliente;
    private double descuento;

    /**
     * Método constructor que inicializa un objeto usuario
     */
    public Usuario() {
    }

    /**
     * Método constructor que inicializa un objeto usuario con datos
     * @param nombre nombre del usuario
     * @param password contraseña del usuario
     * @param saldoTarjeta saldo del usuario
     * @param cliente tipo de cliente que es el usuario
     * @param descuento descuento que se le aplica al usuario
     */
    public Usuario(String nombre, String password, double saldoTarjeta, String cliente, double descuento) {
        this.nombre = nombre;
        this.password = password;
        this.saldoTarjeta = saldoTarjeta;
        this.cliente = cliente;
        this.descuento = descuento;
    }

    /**
     * Método get que muestra el nombre del usuario
     * @return nombre del usuario
     */
    public String getNombre() {
        return nombre;
    }
    
    /**
     * Método set que modifica el nombre del usuario
     * @param nombre nombre del usuario
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    /**
     * Méto get que muestra la contraseña del usuario
     * @return contraseña del usuario
     */
    public String getPassword() {
        return password;
    }

    /**
     * Método set que modifica la contraseña del usuario
     * @param password contraseña del usuario
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Método get que muestra el saldo del usuario
     * @return saldo del usuario
     */
    public double getSaldoTarjeta() {
        return saldoTarjeta;
    }
    
    /**
     * Método set que modifica el saldo del usuario
     * @param saldoTarjeta saldo del usuario
     */
    public void setSaldoTarjeta(double saldoTarjeta) {
        this.saldoTarjeta = saldoTarjeta;
    }

    /**
     * Método get que muestra el tipo de cliente que es el usuario
     * @return tipo de cliente que es el usuario
     */
    public String getCliente() {
        return cliente;
    }

    /**
     * Método set que modifica el tipo de cliente que es el usuario
     * @param cliente tipo de cliente que es el usuario
     */
    public void setCliente(String cliente) {
        this.cliente = cliente;
    }
    
    /**
     * Método get que muestra el descuento que se le aplica al usuario
     * @return descuento que se le aplica al usuario
     */
    public double getDescuento() {
        return descuento;
    }

    /**
     * Método set que modifica el descuento que se le aplica al usuario
     * @param descuento descuento que se le aplica al usuario
     */
    public void setDescuento(double descuento) {
        this.descuento = descuento;
    }
    
    /**
     * Método que disminuye el saldo del usuario
     * @param valorS cantidad a disminuir del saldo
     */
    public void disminuirSaldoT(double valorS) {
        this.saldoTarjeta = this.saldoTarjeta - valorS;
    }
    
    /**
     * Método toString que imprime información del usuario
     * @return nombre, saldo, tipo de cliente y descuento que se le aplica
     */
    @Override 
    public String toString() {
        return "Hola, " + nombre + ", tu saldo inicial es de $" + saldoTarjeta + 
                "\nDebido a que eres cliente " + cliente + " se te aplicará un descuento del " + descuento + "%.";
    }    
    
}