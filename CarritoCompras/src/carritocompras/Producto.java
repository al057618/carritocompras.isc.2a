package carritocompras;

/**
 * Clase para productos
 * @author  Jessica Johana Estefanía Chin Pech 
 * @author  Leslie Alejandra Colli Pech 
 * @author  Karen Estefanía Pérez Pérez 
 * @author  Omar Jasiel Rodriguez Cab 
 * @author  Diana Carolina Uc Vazquez
 * @author  Universidad Autónoma de Campeche
 * @since   Junio 2020
 * @version 3.0
 */
public class Producto {
    
    /**
     * Atributos (nombre, número, cantidad disponible y precio por kg o por l)
     */
    private String nombreProducto;
    private int numProducto;
    private int cantidadDisp;
    private int precioKg;
 
    /**
     * Método constructor que inicializa un objeto producto
     */
    public Producto() {
    }
    
    /**
     * Método constructor que inicializa un objeto producto con datos
     * @param nombreProducto nombre del producto
     * @param numProducto número del producto
     * @param cantidadDisp cantidad disponible del producto
     * @param precioKg precio del producto por kg o por l
     */
    public Producto(String nombreProducto, int numProducto, int cantidadDisp, int precioKg) {
        this.nombreProducto = nombreProducto;
        this.numProducto = numProducto;
        this.cantidadDisp = cantidadDisp;
        this.precioKg = precioKg;
    }

    /**
     * Método get que muestra el nombre del producto
     * @return nombre del producto
     */
    public String getNombreProducto() {
        return nombreProducto;
    }

    /**
     * Método set que modifica el nombre del producto
     * @param nombreProducto nombre del producto
     */
    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    /**
     * Método get que muestra el número del producto
     * @return número del producto
     */
    public int getNumProducto() {
        return numProducto;
    }

    /**
     * Método set que modifica el número del producto
     * @param numProducto número del producto
     */
    public void setNumProducto(int numProducto) {
        this.numProducto = numProducto;
    }
    
    /**
     * Método get que muestra la cantidad discponible del producto
     * @return cantidad disponible del producto
     */
    public int getCantidadDisp() {
        return cantidadDisp;
    }

    /**
     * Método set que modifica la cantidad disponible del producto
     * @param cantidadDisp cantidad disponible del producto
     */
    public void setCantidadDisp(int cantidadDisp) {
        this.cantidadDisp = cantidadDisp;
    }

    /**
     * Método get que muestra el precio por kg o por l del producto
     * @return precio por kg o por l del producto
     */
    public int getPrecioKg() {
        return precioKg;
    }
    
    /**
     * Método set modifica el precio por kg o por l del producto
     * @param precioKg precio por kg o por l del producto
     */
    public void setPrecioKg(int precioKg) {
        this.precioKg = precioKg;
    }
    
    /**
     * Método que disminuye la cantidad disponible del producto
     * @param valorCD cantidad a disminuir de la cantidad disponible del producto
     */
    public void disminuirCant(int valorCD){
        this.cantidadDisp = cantidadDisp - valorCD;
    }

    /**
     * Método toString que imprime información del producto
     * @return nombre, número, cantidad disponible y precio por kg o por l
     */
    @Override
    public String toString() {
        return String.format("%-28s", nombreProducto) + String.format("%-22s", numProducto) 
                + String.format("%-22s", cantidadDisp) + ("$" + precioKg);
    }
    
}