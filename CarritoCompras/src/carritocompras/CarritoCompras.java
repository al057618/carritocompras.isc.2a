package carritocompras;

import java.util.Scanner;

/**
 * Proyecto para agregar productos a un carrito de compras
 * @author  Jessica Johana Estefanía Chin Pech 
 * @author  Leslie Alejandra Colli Pech 
 * @author  Karen Estefanía Pérez Pérez 
 * @author  Omar Jasiel Rodriguez Cab 
 * @author  Diana Carolina Uc Vazquez
 * @author  Universidad Autónoma de Campeche
 * @since   Junio 2020
 * @version 3.0
 */
public class CarritoCompras {
    
    /**
     * Método que imprime el mensaje y salta una línea
     * @param sMsj mensaje a imprimir
     */
    static void imprimirMsjL(String sMsj) {
        System.out.println(sMsj);
    }
    
    /**
     * Método que imprime el mensaje sin saltar una línea
     * @param sMsj mensaje a imprimir
     */
    static void imprimirMsjSinL(String sMsj) {
        System.out.print(sMsj);
    }
    
    /**
     * Método que imprime un separador
     */
    static void separador() {
        imprimirMsjL("------------------------------------------------------------------------------------");
    }
    
    /**
     * Método que imprime un separador doble
     */
    static void dobleSep() {
        imprimirMsjL("====================================================================================");
    }
     
    /**
     * Método que imprime información del programa y de los integrantes del equipo
     */
    static void infoInicial() {        
        dobleSep();
        imprimirMsjL("                         Universidad Autónoma de Campeche");
        imprimirMsjL("                             Facultad de Ingeniería");
        imprimirMsjL("                      Ingeniería en Sistemas Computacionales");
        imprimirMsjL("                          Lenguaje de Programación I, 2A");        
        imprimirMsjL("                          Proyecto 1: Carrito de Compras\n");
        imprimirMsjL("Integrantes del equipo:");
        imprimirMsjL("57039.- Chin Pech Jessica J. Estefanía.");
        imprimirMsjL("56618.- Colli Pech Leslie A.");
        imprimirMsjL("57569.- Pérez Pérez Karen E.");
        imprimirMsjL("56964.- Rodriguez Cab Omar J.");
        imprimirMsjL("57618.- Uc Vazquez D. Carolina.");
        dobleSep();
    }
     
    /**
     * Método que imprime información final (pie de página)
     */
    static void infoFinal() {
        imprimirMsjL("=UAC-FDI-ISC-LDP1-2A-CARRITO-DE-COMPRAS=");
        dobleSep();
    }
    
    /**
     * Método que comienza el programa, declara objetos y sus arreglos
     */
    static void programaCC() {
        Usuario usuario1 = new Usuario("Edgar", "uac", 1500, "plus", 15);
        Usuario usuario2 = new Usuario("Jessica", "estefania28", 1000, "frecuente", 10);       
        Usuario usuario3 = new Usuario("Leslie", "abc", 1000, "frecuente", 10);
        Usuario usuario4 = new Usuario("Karen", "password", 1000, "frecuente", 10);       
        Usuario usuario5 = new Usuario("Omar", "slipknot", 506, "ocasional", 5);   
        Usuario usuario6 = new Usuario("Carolina", "junio", 1000, "frecuente", 10);
        Producto producto1 = new Producto("Carne de res", 101, 20, 40);
        Producto producto2 = new Producto("Pescado", 102, 20, 35);
        Producto producto3 = new Producto("Pechuga de pollo", 103, 30, 25);
        Producto producto4 = new Producto("Huevo", 104, 30, 30);
        Producto producto5 = new Producto("Manzana", 105, 20, 30);
        Producto producto6 = new Producto("Plátano", 106, 20, 15);
        Producto producto7 = new Producto("Pera", 107, 20, 40);
        Producto producto8 = new Producto("Espagueti", 108, 10, 5);
        Producto producto9 = new Producto("Plumilla", 109, 5, 3);
        Producto producto10 = new Producto("Coditos", 110, 10, 5);
        Producto producto11 = new Producto("Leche", 111, 40, 20);
        Producto producto12 = new Producto("Yogurt", 112, 30, 30);
        Producto producto13 = new Producto("Helado", 113, 20, 40);
        Producto producto14 = new Producto("Cerveza", 114, 50, 100);
        Producto producto15 = new Producto("Vino", 115, 15, 150);
        Producto producto16 = new Producto("Vodka", 116, 20, 100);
        Producto producto17 = new Producto("Pinol", 117, 15, 40);
        Producto producto18 = new Producto("Cloro", 118, 15, 30);
        Producto producto19 = new Producto("Ace", 119, 15, 35);
        Producto producto20 = new Producto("Budín", 120, 10, 10);
        Producto producto21 = new Producto("Hojaldra", 121, 10, 15);
        Producto producto22 = new Producto("Dona", 122, 10, 7);
        Usuario usuarioValidable [] = {usuario1, usuario2, usuario3, usuario4, usuario5, usuario6}; 
        Producto productoComprable [] = {producto1, producto2, producto3, producto4, producto5, producto6, 
            producto7, producto8, producto9, producto10, producto11, producto12, producto13, producto14, 
            producto15, producto16, producto17, producto18, producto19, producto20, producto21, producto22};
        menu(usuarioValidable, productoComprable); 
    }
    
    /**
     * Método que contiene el menú para iniciar sesión o salir del programa
     * @param usuarioValidable objetos con datos de los usuarios que pueden ser validados
     * @param productoComprable objetos con datos de los productos que se pueden comprar
     */
    static void menu(Usuario usuarioValidable [], Producto productoComprable []) {
        Scanner tec = new Scanner(System.in);
        imprimirMsjL("                              = CARRITO DE COMPRAS =");
        imprimirMsjL("MENÚ.");
        imprimirMsjL("1.- Iniciar sesión.");
        imprimirMsjL("2.- Salir.");
        separador();
        imprimirMsjSinL("Ingrese el número de opción elegido: ");
        int iOpcion = tec.nextInt();
        separador();
        opMenu(usuarioValidable, productoComprable, iOpcion);
    }
    
    /**
     * Método que realiza la opción del menú deseada 
     * @param usuarioValidable objetos con datos de los usuarios que pueden ser validados
     * @param productoComprable objetos con datos de los productos que se pueden comprar
     * @param iOpcion número entero ingresado para ejecutar la opción correspondiente del menú 
     */
    static void opMenu(Usuario usuarioValidable [], Producto productoComprable [], int iOpcion) {
        switch(iOpcion) {
            case 1:
                imprimirMsjL("INGRESAR.");
                validarUsuario(usuarioValidable, productoComprable);
                break;
            case 2:
                imprimirMsjL("¡Vuelva pronto!");
                dobleSep();
                break;
            default: 
                imprimirMsjL("Opción inexistente.");
                imprimirMsjL("Intente nuevamente.");
                dobleSep();
                menu(usuarioValidable, productoComprable);             
        }
    }

    /**
     * Método que valida el usuario
     * @param usuarioValidable objetos con datos de los usuarios que pueden ser validados
     * @param productoComprable objetos con datos de los productos que se pueden comprar
     */
    static void validarUsuario(Usuario usuarioValidable [], Producto productoComprable []) {
        Scanner tec = new Scanner(System.in);
        imprimirMsjSinL("Usuario: ");
        String sUsuario = tec.nextLine();
        imprimirMsjSinL("Contraseña: ");
        String sContraseña = tec.nextLine();
        int iNoValidado = 0;        
        for(Usuario usuarioValidado:usuarioValidable) {
            if(sUsuario.equals(usuarioValidado.getNombre()) && sContraseña.equals(usuarioValidado.getPassword())) {
                separador();
                System.out.println("Has ingresado correctamente al sistema, " + usuarioValidado.getNombre() + ".");
                imprimirMsjL("Bienvenid@ a tu Carrito de Compras.");
                System.out.println("Tu saldo es de $" + usuarioValidado.getSaldoTarjeta());
                separador();
                listaProductos(usuarioValidable, productoComprable, usuarioValidado);
            }
            else {
                iNoValidado++;
            } 
        }
        if(iNoValidado == 6) {
            separador();
            imprimirMsjL("Usuario incorrecto o contraseña incorrecta.");
            imprimirMsjL("Se le dirigirá al menú principal.");
            dobleSep();
            menu(usuarioValidable, productoComprable);
        }
    }
     
    /**
     * Método que imprime la lista de productos
     * @param usuarioValidable objetos con datos de los usuarios que pueden ser validados
     * @param productoComprable objetos con datos de los productos que se pueden comprar
     * @param usuarioValidado objeto con datos del usuario que fue validado 
     */
    static void listaProductos(Usuario usuarioValidable[], Producto productoComprable [], Usuario usuarioValidado) {
        imprimirMsjL("                             - LISTA DE PRODUCTOS -");
        System.out.println(String.format("%-18s", "     Producto") + String.format("%-23s", "Número de Producto") +
                String.format("%-24s", "Cantidad Disponible") +  "Precio por kg (l)") ;
        for(Producto productoComprar:productoComprable) {
            System.out.println(productoComprar.toString());
        }
        separador();
        submenu(usuarioValidable, productoComprable, usuarioValidado);
    }

    /**
     * Método que contiene el submenú para comprar o cerrar sesión
     * @param usuarioValidable objetos con datos de los usuarios que pueden ser validados
     * @param productoComprable objetos con datos de los productos que se pueden comprar
     * @param usuarioValidado objeto con datos del usuario que fue validado 
     */
    static void submenu(Usuario usuarioValidable[], Producto productoComprable [], Usuario usuarioValidado) {
        Scanner tec = new Scanner(System.in);
        imprimirMsjL("SUBMENÚ.");
        imprimirMsjL("1.- Comprar.");
        imprimirMsjL("2.- Cerrar sesión.");
        separador();
        imprimirMsjSinL("Ingresa el número de opción elegido: ");
        int iOpcion2 = tec.nextInt();
        separador();
        opSubmenu(usuarioValidable, productoComprable, usuarioValidado, iOpcion2);
    }
 
    /**
     * Método que realiza la opción del submenú deseada
     * @param usuarioValidable objetos con datos de los usuarios que pueden ser validados
     * @param productoComprable objetos con datos de los productos que se pueden comprar
     * @param usuarioValidado objeto con datos del usuario que fue validado 
     * @param iOpcion2 número entero ingresado para ejecutar la opción correspondiente del submenú
     */
    static void opSubmenu(Usuario usuarioValidable[], Producto productoComprable [], Usuario usuarioValidado, int iOpcion2) {
        switch(iOpcion2) {
            case 1:
                imprimirMsjL("COMPRAR.");
                System.out.println(usuarioValidado.toString());
                separador();
                comprarProducto(usuarioValidable, productoComprable, usuarioValidado);
                break;
            case 2:
                imprimirMsjL("¡Hasta luego, " + usuarioValidado.getNombre() + "!");                
                dobleSep();
                menu(usuarioValidable, productoComprable);
                break;
            default: 
                imprimirMsjL("Opción inexistente. Intente nuevamente.");
                separador ();
                submenu(usuarioValidable, productoComprable, usuarioValidado);
        }
    }

    /**
     * Método que agrega productos al carrito de cumplir con las condiciones necesarias
     * (número de producto existente, cantidad disponible suficiente, saldo suficiente)
     * @param usuarioValidable objetos con datos de los usuarios que pueden ser validados
     * @param productoComprable objetos con datos de los productos que se pueden comprar
     * @param usuarioValidado objeto con datos del usuario que fue validado 
     */
    static void comprarProducto(Usuario usuarioValidable[], Producto productoComprable [], Usuario usuarioValidado) {
        Scanner tec = new Scanner(System.in);
        double dSaldoInicial = usuarioValidado.getSaldoTarjeta();
        int iImporte;
        double dImporteDesc;
        int iVecesProductoAdq = 0;
        int iTotalKgAdq = 0;
        double dCostoNormal = 0;
        double dDescuento = 0;
        double dCostoDescuento = 0;        
        int iSalir = 0;
        
        do {
            imprimirMsjSinL("Ingresa el número del producto que desea agregar al carrito: ");
            int iNumProducto = tec.nextInt();
            int iNoComprado = 0;
            for(Producto productoComprado:productoComprable) {
                if(iNumProducto == productoComprado.getNumProducto()) {
                    separador();
                    imprimirMsjSinL("Ingresa el número de kilogramos (litros) que deseas de " +
                            productoComprado.getNombreProducto() + " (" + productoComprado.getNumProducto() + "): ");
                    int iNumKg = tec.nextInt();
                    separador();
                    if(iNumKg <= productoComprado.getCantidadDisp()) {
                        iImporte = (iNumKg * productoComprado.getPrecioKg());
                        dImporteDesc = iImporte - (iImporte * usuarioValidado.getDescuento())/100;
                        System.out.println("Precio unitario: $" + productoComprado.getPrecioKg() + "         Importe: $" + 
                                iImporte + "          Importe con descuento: $" + dImporteDesc);
                        separador();
                        if(dImporteDesc <= usuarioValidado.getSaldoTarjeta()) {
                            productoComprado.disminuirCant(iNumKg);
                            usuarioValidado.disminuirSaldoT(dImporteDesc);
                            System.out.println("→ Has añadido " + iNumKg + " kilogramo(s) (litro(s)) del producto " +
                            productoComprado.getNombreProducto() + "(" + productoComprado.getNumProducto() + ") a tu carrito.");
                            System.out.println("→ Saldo restante: $" + usuarioValidado.getSaldoTarjeta());
                            separador();
                            iVecesProductoAdq++;
                            iTotalKgAdq = iTotalKgAdq + iNumKg;
                            dCostoNormal = dCostoNormal + iImporte;
                            dDescuento = (dCostoNormal * usuarioValidado.getDescuento())/100;
                            dCostoDescuento = dCostoDescuento + dImporteDesc;
                        }
                        else {
                            imprimirMsjL("→ No puedes añadir este número de kilogramos (litros) ya que sobrepasaría tu saldo.");
                            System.out.println("→ Saldo restante: $" + usuarioValidado.getSaldoTarjeta());
                            separador();
                        }
                    }
                    else {
                        imprimirMsjL("→ No puedes añadir este número de kg ya que excede a la cantidad disponible.");
                        System.out.println("→ Cantidad disponible: " + productoComprado.getCantidadDisp());
                        separador();
                    }
                }
                else {
                    iNoComprado++;                 
                }
            }
            if(iNoComprado == 22) {
                separador();
                imprimirMsjL("Número de producto inexistente.");
                separador();                
            }
            imprimirMsjL("¿Deseas continuar comprando?");
            imprimirMsjL("1.- Sí, deseo agregar otro producto.");
            imprimirMsjL("2.- No, deseo finalizar mi compra (o un número cualquiera).");
            separador();
            imprimirMsjSinL("Ingresa el número de opción elegido: ");            
            int iOpcion3 = tec.nextInt();
            separador();
            if(iOpcion3 != 1) {
                iSalir++;
            }
        } while (iSalir == 0);
        
        if(iVecesProductoAdq > 0) {
            resumenCompra(usuarioValidable, productoComprable, usuarioValidado, dSaldoInicial,
                    iVecesProductoAdq, iTotalKgAdq, dCostoNormal, dDescuento, dCostoDescuento);
        }
        else {
            imprimirMsjL("No has agregado productos a tu carrito de compras.");
            imprimirMsjL("Se te dirigirá al submenú.");
            separador();
            submenu(usuarioValidable, productoComprable, usuarioValidado);
        }
        
    }
    
    /**
     * Método que imprime el resumen de compra
     * @param usuarioValidable objetos con datos de los usuarios que pueden ser validados
     * @param productoComprable objetos con datos de los productos que se pueden comprar
     * @param usuarioValidado objeto con datos del usuario que fue validado 
     * @param dSaldoInicial variable que almacena el saldo anterior a la compra 
     * @param iVecesProductoAdq variable que almacena las veces que se adquirió algún producto
     * @param iTotalKgAdq variable que almanacena el total de kg o l adquiridos
     * @param dCostoNormal variable que almacena el costo total normal, es decir, sin descuento
     * @param dDescuento variable que almacena el descuento hecho
     * @param dCostoDescuento variable que almacena el costo total con descuento
     */
    static void resumenCompra(Usuario usuarioValidable[], Producto productoComprable [], Usuario usuarioValidado, double dSaldoInicial,
                int iVecesProductoAdq, int iTotalKgAdq, double dCostoNormal, double dDescuento, double dCostoDescuento){
        imprimirMsjL("¡Enhorabuena, " + usuarioValidado.getNombre() + ", tu compra fue realizada con éxito!");
        separador();
        imprimirMsjL("RESUMEN DE COMPRA.");
        System.out.println("Saldo inicial: $" + dSaldoInicial);
        System.out.println("Total de veces que se agregó un producto al carrito: " + iVecesProductoAdq);
        System.out.println("Total de kilogramos (litros) agregados al carrito: " + iTotalKgAdq);
        System.out.println("Subtotal(costo inicial de los productos): $" + dCostoNormal);
        System.out.println("Descuento (" + usuarioValidado.getDescuento() + "%): $" + dDescuento);
        System.out.println("Total (costo final con descuento aplicado): $" + dCostoDescuento);
        System.out.println("Saldo final: $" + usuarioValidado.getSaldoTarjeta());
        separador();
        System.out.println("¡Gracias por tu compra, " + usuarioValidado.getNombre() + "!");
        imprimirMsjL("Se te dirigirá al submenú.");            
        separador();
        submenu(usuarioValidable, productoComprable, usuarioValidado);
    }
    
    /**
     * Método que permite ejecutar el programa
     * @param args 
     */
    public static void main(String[] args) {
        infoInicial();
        programaCC();
        infoFinal();
    }    
}